import './App.css';
import Home from './components/Home';
import Info from './components/Info';
import {useEffect, useState} from 'react';
import {
  BrowserRouter as Router,
  Routes,
  Route
} from "react-router-dom";

function App() {

  // const [pnr,setPnr] = useState();

 
  return (
    // <div>
    // <Home/>
    // <Info/>
    // </div>
    

    <Router>
      <Routes>
        <Route path="/" element={<Home />} />
        <Route path="/info" element={<Info />} />
      </Routes>
    </Router>
  );
}

export default App;
