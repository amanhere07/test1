import React from 'react' 
import Navbar from './Navbar'
import {Component} from 'react';
import {Button} from '@mui/material'
import axios from 'axios'
function Info()
{

    let formData = new FormData();

    let pnr = localStorage.getItem('pnr');


    const onFileChange= (e)=>{
        console.log(e.target.files[0]);
        if(e.target && e.target.files[0]){
            formData.append('name','Hello');
            formData.append('file',e.target.files[0]);
            
        }
    }

    const SubmitFileData=()=>{
        axios.post('http://10.15.74.45:8080/upload',{formData}
        )
        .then(res=>{
            console.log(res);
        })
        .catch(e=>{
            console.log(e);
        })
    }

    return(
       
       <div >
            <Navbar/>
                <input type="file" name="identity" onChange={onFileChange} />
                <br/>
                <input type="file" name="vaccineCerti" onChange={onFileChange} />
                <button onClick={SubmitFileData}>Submit Data</button>
        </div>
    )
}
export default Info;